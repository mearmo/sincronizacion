#!/usr/bin/env python

#import libraries
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import csv
import sys 

'''PREP'''
file = sys.argv[1] 
#read data from csv
data = pd.read_csv(file, usecols=['population','weeks','red','green','gray'])
population = data['population']
weeks = data['weeks']
red = data['red']
green = data['green']
gray = data['gray']

plt.style.use('ggplot')

plt.plot(weeks, population, color='#0E11E1', label='Población Total')
plt.plot(weeks, red, color='#E1110E', label='Infectados')
plt.plot(weeks, green, color='#14CD11', label='Sanos')
plt.plot(weeks, gray, color='#4B5A4B', label='Inmunes')

plt.title('Virus')
plt.ylabel('Cantidad de personas')
plt.xlabel('Semanas')

plt.legend(loc='upper right')

plt.tight_layout()

plt.show()
