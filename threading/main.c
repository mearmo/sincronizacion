#include "Virus.c"
#define NUM_THREADS 300

int main(int argc, char * argv []) {
    srand(time(NULL));

    pthread_t threads[NUM_THREADS];


    //read parameters
    for (size_t i = 1; i < argc; i++) {
        if(strcmp(argv[i],"-n")==0){args.number_people = atoi(argv[i+1]);}
        else if(strcmp(argv[i],"-i")==0){args.infectiousness = atoi(argv[i+1]);}
        else if(strcmp(argv[i],"-c")==0){args.chance_recover = atoi(argv[i+1]);}
        else if(strcmp(argv[i],"-d")==0){args.duration = atoi(argv[i+1]);}
        else if(strcmp(argv[i],"-w")==0){args.weeks = atoi(argv[i+1]);}
    }

    //start dead until creation
    for (int i = 0; i < CAPACITY; i++){
        tortugas[i].dead = true;
    }

    //create first population
    for (int i = 0; i < args.number_people; i++){
        create_turtle(i);
        tortugas[i].age = rand()%LIFESPAN; //first with random age
        if (i < 10){tortugas[i].sick = true;} // 10 get sick at the beginning 
    }

    //open file to save data
    FILE *f=fopen("datos.csv","w");
    fprintf(f,"population,weeks,red,green,gray\n");

    int j = 0;
    while (j < args.weeks){ //do this j amount of weeks
        int t_ctr = 0;
        for (int k = 0; k < CAPACITY; k++){
            if(tortugas[k].dead == false){
                struct thread_args in = { .a = k};
                pthread_create(&threads[t_ctr], NULL, get_older, &in);
                t_ctr++;
            }
        }

        for(int k =0; k<t_ctr;k++){
            void* status;
            pthread_join(threads[k], &status);
        }
        

        t_ctr = 0;
        for (int k = 0; k < CAPACITY; k++){
            if(tortugas[k].dead == false){
                struct thread_args in = { .a = k};
                pthread_create(&threads[t_ctr], NULL, move, &in);
                t_ctr++;
            }
        }
        
        for(int k =0; k<t_ctr;k++){
            void* status;
            pthread_join(threads[k], &status);
        }



        t_ctr = 0;
        for (int k = 0; k < CAPACITY; k++){
            if(tortugas[k].sick == false && tortugas[k].dead == false ){ //if not sick and repoduction, create new
                int random = rand()%100;
                if (population < CAPACITY && random <= REPRODUCE){
                    struct thread_args in = { .a = k};
                    pthread_create(&threads[t_ctr], NULL, search_empty_life, &in);
                    t_ctr++;
                }
            }
        }

        for(int k =0; k<t_ctr;k++){
            void* status;
            pthread_join(threads[k], &status);
        }


        
        
        t_ctr = 0;
        for (int i = 0; i < CAPACITY; i++){ //if it's sick, try to infect the others on your same position
            if (tortugas[i].sick == true && tortugas[i].dead == false){
                struct thread_args in = { .a = i};
                pthread_create(&threads[t_ctr], NULL, search_position, &in);
                t_ctr++;
            }
        }
        for(int k =0; k<t_ctr;k++){
            void* status;
            pthread_join(threads[k], &status);
        }




       red = 0;
       green=0;
       gray=0;

		struct contador in1 = { 
			.arraypos1=0,
			.arraypos2=CAPACITY/2
			};
        struct contador in2 = { 
			.arraypos1=CAPACITY/2,
			.arraypos2=CAPACITY
			};
		pthread_create(&threads[0], NULL, count, &in1);
		pthread_create(&threads[1], NULL, count, &in2);
				
  
		pthread_join(threads[0], NULL);
		pthread_join(threads[1], NULL);

        j++;
        fprintf(f,"%d,%d,%d,%d,%d\n",population,j,red,green,gray); //save data in file
    }
    
    fclose(f); //close file
    return 0;
}
