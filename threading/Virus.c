#include "Virus.h"

#define LIFESPAN 2600 //50 YEARS * 52 WEEKS
#define CAPACITY 300 //The number of turtles/people that can be in the world at one time
#define IMMUNUTY 52 //1 YEAR
#define REPRODUCE 1 
#define GRID 20 // grid 100*100

struct turtle tortugas[CAPACITY];
struct argumentos args;
int population = 0;
int red = 0;
int green = 0;
int gray = 0;
// pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

//create a turtle healthy, with no immunity, at a random place, age 0.
void create_turtle(int index){
    tortugas[index].sick = false;
    tortugas[index].sick_time = 0;
    tortugas[index].remaining_immunity = 0;
    tortugas[index].age = 0;
    tortugas[index].x = rand()%GRID;
    tortugas[index].y = rand()%GRID;
    tortugas[index].dead = false;
    pthread_mutex_lock(&mutex);
    population ++;
    pthread_mutex_unlock(&mutex);
}

//if sick
void get_sick(int index){
    pthread_mutex_lock(&mutex);
    tortugas[index].sick = true;
    tortugas[index].remaining_immunity = 0;
    pthread_mutex_unlock(&mutex);
}

//if recover
void get_healthy(int index){
    tortugas[index].sick = false;
    tortugas[index].sick_time = 0;
    tortugas[index].remaining_immunity = IMMUNUTY;
}

//getting older. Dead if age is bigger dan lifespan. Ask if recover or die depending on the sicktime
void *get_older(void *args_void){
    // printf("Enter 1\n");
    // pthread_mutex_lock(&mutex);
    struct thread_args *argus = args_void;
    int index = argus->a;
    
    tortugas[index].age ++;
    if (tortugas[index].age > LIFESPAN){
        tortugas[index].dead = true;
        pthread_mutex_lock(&mutex);
        population --;
        pthread_mutex_unlock(&mutex);
    }
    if (tortugas[index].remaining_immunity > 0){tortugas[index].remaining_immunity --;}
    if (tortugas[index].sick == true){
        pthread_mutex_lock(&mutex);
        tortugas[index].sick_time ++;
        recover_die(index);
        pthread_mutex_unlock(&mutex);
    }
    pthread_exit(0);
    // pthread_mutex_unlock(&mutex);
    // printf("Thread 1\n");
}

//8 posible positions to move. Up, down, left, right and diagonals. Move just accross the GRID size
void *move(void *args_void){
    // printf("Enter 2\n");
    // pthread_mutex_lock(&mutex);
    struct thread_args *argus = args_void;
    int index = argus->a;
    int random = rand()%8;
    switch( random )
    {
        case 0:
            if(tortugas[index].x == 0){tortugas[index].x = GRID;}
            else{tortugas[index].x --;}
            if(tortugas[index].y == 0){tortugas[index].y = GRID;}
            else{tortugas[index].y --;}
        case 1:
            if(tortugas[index].x == 0){tortugas[index].x = GRID;}
            else{tortugas[index].x --;}
        case 2:
            if(tortugas[index].x == 0){tortugas[index].x = GRID;}
            else{tortugas[index].x --;}
            if(tortugas[index].y == GRID){tortugas[index].y = 0;}
            else{tortugas[index].y ++;}
        case 3:
            if(tortugas[index].y == GRID){tortugas[index].y = 0;}
            else{tortugas[index].y ++;}
        case 4:
            if(tortugas[index].x == GRID){tortugas[index].x = 0;}
            else{tortugas[index].x ++;}
            if(tortugas[index].y == GRID){tortugas[index].y = 0;}
            else{tortugas[index].y ++;}
        case 5:
            if(tortugas[index].x == GRID){tortugas[index].x = 0;}
            else{tortugas[index].x ++;}
        case 6:
            if(tortugas[index].x == GRID){tortugas[index].x = 0;}
            else{tortugas[index].x ++;}
            if(tortugas[index].y == 0){tortugas[index].y = GRID;}
            else{tortugas[index].y --;}
        case 7:
            if(tortugas[index].y == 0){tortugas[index].y = GRID;}
            else{tortugas[index].y --;}
    }

    pthread_exit(0);
    // printf("Thread 2\n");
    // pthread_mutex_unlock(&mutex);

}

//trying to infect another turtle
void infect(int index){
    if (tortugas[index].sick == false && tortugas[index].remaining_immunity == 0){
        int random = rand()%100;
        if (random <= args.infectiousness){get_sick(index);}
    }
}

//if sicktime is bigger than duration, recover o die depending on chance of recovery
void recover_die(int index){
    if (tortugas[index].sick_time > args.duration){
        int random = rand()%100;
        if (random <= args.chance_recover){get_healthy(index);}
        else{
            tortugas[index].dead = true;
            population --;
        }
    }
}



void *search_empty_life(void *args_void){
    for (int w = 0; w < CAPACITY; w++){
        if (tortugas[w].dead == true){ //if there's space, find one with no living turtle
            create_turtle(w);
            break;
        }
    }
}


void *search_position(void *args_void){
    struct thread_args *argus = args_void;
    int i = argus->a;
    for (int k = 0; k < CAPACITY; k++){
        if(tortugas[i].x == tortugas[k].x && tortugas[i].y == tortugas[k].y){
            if (tortugas[k].dead == false){

                infect(k);
                // struct thread_args in = { .a = k};
                // pthread_create(&threads[t_ctr], NULL, infect, &in);
                // t_ctr++;
            }
        }
    }
}

void *count(void *args_void){
	pthread_mutex_lock(&mutex);
	struct contador *argus = args_void;
	int arraypos1=argus->arraypos1;
	int arraypos2=argus->arraypos2;
	// red=0;
	// green=0;
	// gray=0;
    // printf("%d|%d\n", arraypos1, arraypos2);
	for (int i = arraypos1; i < arraypos2; i++){ //counters
		
		if(tortugas[i].sick == true && tortugas[i].dead == false){red ++;}
		else if(tortugas[i].remaining_immunity == 0 && tortugas[i].dead == false){green ++;}
		else if(tortugas[i].remaining_immunity > 0 && tortugas[i].dead == false){gray ++;}
     }
     pthread_mutex_unlock(&mutex);
    
    pthread_exit(0);
	
}
