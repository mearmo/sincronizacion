# Introducción a la programación paralela

Proyecto 2 IE0521 
Paradigma de programación paralela por medio de pthreads para un modelo basado en simulación de agentes. Esta simulación modela sistema complejos a partir de agente autónomos que interactúan con el ambiente yentre sí con comportamientos y reglas ya definidas. Se utiliza como base el modelo del virus de Netlogo y se implementa en C. Las gráficas se realizan con Python. 

## Compliar el programa 
Para compilar el programa se utiliza:
```
>> make build
```
## Ejecución del programa
Para ejecutar el programa se utiliza:
```
>> make run
```
## Visualiazción de resultados
Para mostarar las gráficas de los resultados:
```
>> make show
```
## Todo
Para compilar, ejecutar y mostrar los resultados:
```
>> make all
```