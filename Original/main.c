#include "Virus.c"
// #define NUM_THREADS 5

int main(int argc, char * argv []) {
    srand(time(NULL));

    // pthread_t threads[NUM_THREADS];

    //read parameters
    for (size_t i = 1; i < argc; i++) {
        if(strcmp(argv[i],"-n")==0){args.number_people = atoi(argv[i+1]);}
        else if(strcmp(argv[i],"-i")==0){args.infectiousness = atoi(argv[i+1]);}
        else if(strcmp(argv[i],"-c")==0){args.chance_recover = atoi(argv[i+1]);}
        else if(strcmp(argv[i],"-d")==0){args.duration = atoi(argv[i+1]);}
        else if(strcmp(argv[i],"-w")==0){args.weeks = atoi(argv[i+1]);}
    }

    //start dead until creation
    for (int i = 0; i < CAPACITY; i++){
        tortugas[i].dead = true;
    }

    //create first population
    for (int i = 0; i < args.number_people; i++){
        // struct thread_args in = { .a = i};
        create_turtle(i);
        tortugas[i].age = rand()%LIFESPAN; //first with random age
        if (i < 10){tortugas[i].sick = true;} // 10 get sick at the beginning 
    }

    //open file to save data
    FILE *f=fopen("datos.csv","w");
    fprintf(f,"population,weeks,red,green,gray\n");

    int j = 0;
    while (j < args.weeks){ //do this j amount of weeks
    int c1 = 0;
    int c2 = 0;
        for (int k = 0; k < CAPACITY; k++){
            if(tortugas[k].dead == false){
                get_older(k);
                move(k);
 
            }
        }
        

        for (int k = 0; k < CAPACITY; k++){
            if(tortugas[k].sick == false && tortugas[k].dead == false ){ //if not sick and repoduction, create new
                int random = rand()%100;
                if (population < CAPACITY && random <= REPRODUCE){
                    for (int w = 0; w < CAPACITY; w++){
                        if (tortugas[w].dead == true){ //if there's space, find one with no living turtle
                            create_turtle(w);
                            break;
                        }
                    }
                }
            }
        }

        
        

        for (int i = 0; i < CAPACITY; i++){ //if it's sick, try to infect the others on your same position
            if (tortugas[i].sick == true && tortugas[i].dead == false){
                for (int k = 0; k < CAPACITY; k++){
                    if(tortugas[i].x == tortugas[k].x && tortugas[i].y == tortugas[k].y){
                        if (tortugas[k].dead == false){infect(k);}
                    }
                }
            }
        }

        int red = 0;
        int green = 0;
        int gray = 0;

        for (int i = 0; i < CAPACITY; i++){ //counters
            if(tortugas[i].sick == true && tortugas[i].dead == false){red ++;}
            else if(tortugas[i].remaining_immunity == 0 && tortugas[i].dead == false){green ++;}
            else if(tortugas[i].remaining_immunity > 0 && tortugas[i].dead == false){gray ++;}
        }

        j++;
        fprintf(f,"%d,%d,%d,%d,%d\n",population,j,red,green,gray); //save data in file
    }
    
    fclose(f); //close file
    return 0;
}
