#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>
#include <pthread.h>

#ifndef VIRUS_H
#define VIRUS_H


struct turtle {
    bool sick; //if true, the turtle is infectious
    int remaining_immunity; //how many weeks of immunity the turtle has left
    int sick_time; //how long, in weeks, the turtle has been infectious
    int age; //how many weeks old the turtle is
    int x; //coordinates on the grid
    int y; 
    bool dead;
};

struct argumentos {
    int number_people;
    int infectiousness;
    int chance_recover;
    int duration;
    int weeks;
};

struct thread_args{
    int a;
};

void create_turtle(int index);

void get_sick(int index);

void get_healthy(int index);

void get_older(int index);

void move(int index);

void infect(int index);

void recover_die(int index);


#endif