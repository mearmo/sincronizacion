#include "Virus.h"

#define LIFESPAN 2600 //50 YEARS * 52 WEEKS
#define CAPACITY 300 //The number of turtles/people that can be in the world at one time
#define IMMUNUTY 52 //1 YEAR
#define REPRODUCE 1 
#define GRID 20 // grid 100*100

struct turtle tortugas[CAPACITY];
struct argumentos args;
int population = 0;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

//create a turtle healthy, with no immunity, at a random place, age 0.
void create_turtle(int index){
    // pthread_mutex_lock(&mutex);
    tortugas[index].sick = false;
    tortugas[index].sick_time = 0;
    tortugas[index].remaining_immunity = 0;
    tortugas[index].age = 0;
    tortugas[index].x = rand()%GRID;
    tortugas[index].y = rand()%GRID;
    tortugas[index].dead = false;
    population ++;
    // pthread_mutex_unlock(&mutex);
}

//if sick
void get_sick(int index){
    tortugas[index].sick = true;
    tortugas[index].remaining_immunity = 0;
}

//if recover
void get_healthy(int index){
    tortugas[index].sick = false;
    tortugas[index].sick_time = 0;
    tortugas[index].remaining_immunity = IMMUNUTY;
}

//getting older. Dead if age is bigger dan lifespan. Ask if recover or die depending on the sicktime
void get_older(int index){

    
    tortugas[index].age ++;
    if (tortugas[index].age > LIFESPAN){
        tortugas[index].dead = true;
        population --;
    }
    if (tortugas[index].remaining_immunity > 0){tortugas[index].remaining_immunity --;}
    if (tortugas[index].sick == true){
        tortugas[index].sick_time ++;
        recover_die(index);
    }
    // pthread_mutex_unlock(&mutex);
    // printf("Thread 1\n");
}

//8 posible positions to move. Up, down, left, right and diagonals. Move just accross the GRID size
void move(int index){
    // printf("Enter 2\n");
    // pthread_mutex_lock(&mutex);
    int random = rand()%8;
    switch( random )
    {
        case 0:
            if(tortugas[index].x == 0){tortugas[index].x = GRID;}
            else{tortugas[index].x --;}
            if(tortugas[index].y == 0){tortugas[index].y = GRID;}
            else{tortugas[index].y --;}
        case 1:
            if(tortugas[index].x == 0){tortugas[index].x = GRID;}
            else{tortugas[index].x --;}
        case 2:
            if(tortugas[index].x == 0){tortugas[index].x = GRID;}
            else{tortugas[index].x --;}
            if(tortugas[index].y == GRID){tortugas[index].y = 0;}
            else{tortugas[index].y ++;}
        case 3:
            if(tortugas[index].y == GRID){tortugas[index].y = 0;}
            else{tortugas[index].y ++;}
        case 4:
            if(tortugas[index].x == GRID){tortugas[index].x = 0;}
            else{tortugas[index].x ++;}
            if(tortugas[index].y == GRID){tortugas[index].y = 0;}
            else{tortugas[index].y ++;}
        case 5:
            if(tortugas[index].x == GRID){tortugas[index].x = 0;}
            else{tortugas[index].x ++;}
        case 6:
            if(tortugas[index].x == GRID){tortugas[index].x = 0;}
            else{tortugas[index].x ++;}
            if(tortugas[index].y == 0){tortugas[index].y = GRID;}
            else{tortugas[index].y --;}
        case 7:
            if(tortugas[index].y == 0){tortugas[index].y = GRID;}
            else{tortugas[index].y --;}
    }
    // printf("Thread 2\n");
    // pthread_mutex_unlock(&mutex);

}

//trying to infect another turtle
void infect(int index){
    if (tortugas[index].sick == false && tortugas[index].remaining_immunity == 0){
        int random = rand()%100;
        if (random <= args.infectiousness){get_sick(index);}
    }
}

//if sicktime is bigger than duration, recover o die depending on chance of recovery
void recover_die(int index){
    if (tortugas[index].sick_time > args.duration){
        int random = rand()%100;
        if (random <= args.chance_recover){get_healthy(index);}
        else{
            tortugas[index].dead = true;
            population --;
        }
    }
}
